from web_app            import *
from web_app.models     import *
from web_app.le.utility import validate_default_admin, validate_session
from web_app.forms      import OrganizationForm, EditOrganizationForm, CertificateForm
from flask              import render_template, request, make_response, redirect, url_for, flash, session
from flask_login        import LoginManager, login_user, logout_user, login_required
from datetime           import datetime


import time, json
import linecache
import sys

@app.route('/', methods=['POST', 'GET'])
def index():

    session['logged_in'] = False
    session['wrong_pass'] = True

    http_response = make_response(render_template('index.html', session=session, logged_in=False))
    if not validate_session(request):
        if request.method == "POST": 
            try:
                if request.form['organization'] == "Default":
                    if validate_default_admin(request.form):
                        session['logged_in']  = True
                        session['wrong_pass'] = False
                        
                        org  = Organization.query.filter_by(organization_name="Default").first()
                        user = User.query.filter_by(username="admin", org_id=org.id).first() 

                        login_user(user)
                        http_response = make_response(render_template('index.html', 
                                                                      session=session, 
                                                                      logged_in=True,
                                                                      org_name=request.form['organization']))
                        session['org_name'] = "Default"
                else:
                    org = Organization.query.filter_by(organization_name=request.form['organization']).first()
                    if org:
                        user = User.query.filter_by(username=request.form['username'], org_id=org.id).first()
                        if user and user.verify_password(request.form['password']):
                
                            attempted_username = request.form['username']
                            attempted_password = request.form['password']
                        
                            session['logged_in']  = True
                            session['wrong_pass'] = False

                            login_user(user)
                            http_response = make_response(render_template('index.html', 
                                                                          session=session, 
                                                                          logged_in=True, 
                                                                          org_name=request.form['organization']))
                            session['org_name'] = request.form['organization']

            except Exception as e:
                print(repr(e))

    return http_response

@app.route('/request_certificate', methods=['GET', 'POST'])
@login_required
def req_cert():

    form = CertificateForm(request.form)
    if request.method == "GET":
        if request.method == "POST":
            return
        http_response = make_response(render_template('request_certificate.html', 
                                      logged_in=True, 
                                      cert_form=form, 
                                      org_name=session['org_name']))
    return http_response

@app.route('/provisioned_certificates', methods=['GET'])
@login_required
def provisioned_certificates():
    
    try:
        
        org           = Organization.query.filter_by(organization_name=session['org_name']).first()
        certificates  = Certificate.query.filter_by(org_id=org.id).all()
        http_response = make_response(render_template('provisioned_certificates.html', certificates=certificates,  logged_in=True, org_name=session['org_name']))
        
    except Exception as e:
        return redirect(url_for('index')) 
 
    return http_response

@app.route('/admin', methods=['POST', 'GET'])
@login_required
def admin_portal():
    return render_template('admin.html', selections=None,  logged_in=True, org_name=session['org_name'])
   

@app.route('/admin_account', methods=['POST', 'GET'])
@login_required
def set_admin_account_credentials():
    if request.method == "POST":
        def_org                  = Organization.query.filter_by(organization_name=org_name).first()
        admin_user               = User.query.filter_by(username="admin", org_id=def_org.id).first()
        admin_user.password_hash = generate_password_hash(request.form['password'])

        db.session.add(admin_user)
        db.session.commit()

        return render_template('admin.html', selection="admin_account",  logged_in=True, account_updated=True, org_name=session['org_name'])

    return render_template('admin.html', selection="admin_account",  logged_in=True, account_updated=False, org_name=session['org_name'])


@app.route('/created_user', methods=['POST', 'GET'])
@login_required
def create_new_user():
    return

@app.route('/view_edit_users', methods=['POST', 'GET'])
@login_required
def view_edit_all_users():
    return

@app.route('/remove_user', methods=['POST', 'GET'])
@login_required
def remove_users():
    return

@app.route('/create_organization', methods=['POST', 'GET'])
@login_required
def create_new_organization():
    
    org = OrganizationForm(request.form)
    http_response = None
    if request.method == "POST":
        print(org.errors)
        if org.validate():
            
            try:
                new_org_record = Organization(organization_name=org.organization_name.data, created=datetime.now())
                db.session.add(new_org_record)
                db.session.commit()
            except Exception as e:
                db.session.rollback()

            org_admin_role = Role.query.filter_by(name="org_admin_role").first()
            org_admin      = User(username="admin", 
                                password_hash=generate_password_hash(org.org_default_admin_password.data), 
                                role=org_admin_role,
                                organization=new_org_record)
            db.session.add(org_admin)
            db.session.commit()
        
            return render_template('admin.html', form=org, selection="create_organization", new_org=True,  logged_in=True, org_name=session['org_name'])
    
    return render_template('admin.html', form=org, selection="create_organization", new_org=False, org_name=session['org_name'],  logged_in=True)
    

@app.route('/view_edit_organizations', methods=['POST', 'GET'])
@login_required
def view_edit_all_organizations():
    
    http_response = None
    orgs     = Organization.query.all()
    org_form = OrganizationForm(request.form)
    if request.method == "POST":
        org_form                 = EditOrganizationForm(request.form)
        org_name                 = request.form['update']

        print(org_name)
        http_response            = render_template('admin.html', 
                                                    selection="view_organizations", 
                                                    org_list=orgs, 
                                                    crud_org_modal=True, 
                                                    form=org_form, 
                                                    org_name=session['org_name'],
                                                    edit_org_name=request.form['update'],  
                                                    logged_in=True)
    else:
        http_response = render_template('admin.html', selection="view_organizations", org_list=orgs, crud_org_modal=False, org_name=session['org_name'],  logged_in=True)
    
    return http_response
    
@app.route('/edit_organizations/<edit_org_name>', methods=['POST', 'GET'])
@login_required
def edit_organization(edit_org_name):
    
    org_form      = EditOrganizationForm(request.form)
    if request.form['submit'] == "Submit":
        if org_form.delete_org.data:
            org = Organization.query.filter_by(organization_name=edit_org_name).first()
            db.session.delete(org)
            db.session.commit()
        else:
            print("LLLLLL")
            org = Organization.query.filter_by(organization_name=edit_org_name).first()
            if org_form.organization_name.data:
                org.organization_name = org_form.organization_name.data

                db.session.add(org)
                db.session.commit()

            default_org_admin = User.query.filter_by(username=org_form.org_default_hidden_username.data).first()
            if org_form.org_default_admin_password.data:
                default_org_admin.password_hash = generate_password_hash(org_form.org_default_admin_password.data)

            db.session.add(default_org_admin)
            db.session.commit()

        return redirect(url_for('view_edit_all_organizations'))
    else:
        return redirect(url_for('view_edit_all_organizations'))

@app.route('/log_out', methods=['POST', 'GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index')) 