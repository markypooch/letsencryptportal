from web_app        import *
from web_app.models import *
from datetime       import datetime

def validate_default_admin(request_form):
    validated_successful = False
    if request_form['username'] == "admin":
        
        # first() will return the top result, or none if there are no results
        org_row      = Organization.query.filter_by(organization_name="Default").first()
        role_row     = Role.query.filter_by(name="Super_Admin").first()
        user_row     = User.query.filter_by(username="admin").first()

        print("Heto!")

        if all(row is not None for row in (org_row, role_row, user_row)):
            print(request_form['password'])
            if user_row.verify_password(request_form['password']):
                validated_successful = True
                print("WOOOOOT!")
            else:
                print(": (")

        else:
            print("Creating admin credentials")
            if request_form['password'] == "password":
                if not org_row:
                    org = Organization(organization_name="Default", created=datetime.now())
                    db.session.add(org)
                    print("Created, 'DEFAULT' Org")
                    db.session.commit()

                if not role_row:
                    role = Role(name="Super_Admin")
                    db.session.add(role)

                    org_admin_role = Role(name="Org_Admin")
                    db.session.add(role)

                    print("Created, 'Super_Admin' Role")

                    db.session.commit()

                if not user_row:
                    
                    org_ref, role_ref = None, None

                    org_ref = org_row  if org_row else org
                    row_ref = role_row if role_row else role
                    
                    u = User(username="admin", password_hash=generate_password_hash("password"), role=role_ref, organization=org_ref)
                    db.session.add(u)
                    db.session.commit()
                    print("Created, 'Administrator' User")

                validated_successful = True
    return validated_successful

def validate_session(request):
    session_cookie = request.cookies.get('le_cookie')
    if session_cookie:
        return True
    return False