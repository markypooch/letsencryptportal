from web_app                   import *
from web_app.views.views       import *
from web_app.views.admin_views import *
from web_app                   import models

if __name__ == "__main__":
    app.run(debug=True, port="12222")