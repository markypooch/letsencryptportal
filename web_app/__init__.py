from flask import Flask
from flask_assets     import Bundle, Environment
from flask_sqlalchemy import SQLAlchemy
from flask_login      import LoginManager
import os

app = Flask(__name__)
env = Environment(app)
js = Bundle('js/clarity-icons.min.js', 'js/clarity-icons-api.js',
            'js/clarity-icons-element.js', 'js/custom-elements.min.js',
            'js/letsencrypt-show-nsx.js')
env.register('js_all', js)
css = Bundle('css/clarity-ui.min.css', 'css/clarity-icons.min.css')
env.register('css_all', css)

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = "Derp"
db = SQLAlchemy(app)

login_manager = LoginManager(app)
login_manager.login_view = 'index'

## GLOBALS


org_name = ""