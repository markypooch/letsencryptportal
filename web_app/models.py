from web_app           import *
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login       import LoginManager, UserMixin

class Role(db.Model):
    id    = db.Column(db.Integer, primary_key=True)
    name  = db.Column(db.String(64), unique=True)
    users = db.relationship('User', backref='role')


@login_manager.user_loader
def load_user(user_id):
    return db.session.query(User).get(user_id)

class User(db.Model, UserMixin):

    id            = db.Column(db.Integer, primary_key=True)
    username      = db.Column(db.String(256))
    password_hash = db.Column(db.String(128))
    role_id       = db.Column(db.Integer, db.ForeignKey('role.id'))
    org_id        = db.Column(db.Integer, db.ForeignKey('organization.id'))

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)
    
class Organization(db.Model):
    id                 = db.Column(db.Integer, primary_key=True)
    organization_name  = db.Column(db.String(256), unique=True)
    created            = db.Column(db.DateTime)
    users = db.relationship('User', backref='organization')

class Certificate(db.Model):

    id                     = db.Column(db.Integer, primary_key=True)
    org_id                 = db.Column(db.Integer, db.ForeignKey('organization.id'))
    description            = db.Column(db.String(512))
    certificate_names      = db.Column(db.String(4098))
    provisioned_timestamp  = db.Column(db.DateTime)
    renewal_timestamp      = db.Column(db.DateTime)
    pre_deploy_script      = db.Column(db.String(10512))
    post_deploy_script     = db.Column(db.String(10512))