from flask_wtf import FlaskForm
from wtforms   import StringField, SubmitField, PasswordField, BooleanField, HiddenField, TextAreaField
from wtforms.validators import DataRequired, EqualTo

class OrganizationForm(FlaskForm):
    organization_name              = StringField("Organization Name", validators=[DataRequired()])
    org_default_admin_username     = StringField("Default Admin Username")
    org_default_admin_password     = PasswordField("Default Admin Password", validators=[DataRequired()])
    con                            = PasswordField("Confirm Password", validators=[DataRequired()])
    submit                         = SubmitField('Create Organization')

class EditOrganizationForm(FlaskForm):
    organization_name              = StringField("Organization Name")
    org_default_admin_username     = StringField("Default Admin Username")
    org_default_admin_password     = PasswordField("Default Admin Password")
    con                            = PasswordField("Confirm Password")
    delete_org                     = BooleanField("Delete this org")

class CertificateForm(FlaskForm):
    description                    = StringField("Description")
    certificate_names              = StringField("Certificate Names")

    pre_deployment_script          = TextAreaField("Pre Deployment Script")
    post_deployment_script         = TextAreaField("Post Deployment Script")

    error_email_address            = StringField("Email Address")