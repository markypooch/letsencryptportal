FROM centos:7

WORKDIR /
COPY    . /

RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*

RUN yes | yum update
RUN yes | yum install yum-utils
RUN yes | yum groupinstall development

RUN yes | yum install https://centos7.iuscommunity.org/ius-release.rpm
RUN yes | yum install python36u

RUN yes | yum install python36u-pip
RUN yes | yum install python36u-devel

RUN yes | yum install python-devel openldap-devel

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "main.py"]
